PACKAGE_NAME :=
INSTRUCTION :=

install:
ifndef PACKAGE_NAME
	@echo "Wymage podanie nazwy paczki npm."
	@exit 1
endif
	npm install $(PACKAGE_NAME)@latest --force

update-package:
ifndef PACKAGE_NAME
	@echo "Wymagane podanie nazwy paczki npm."
	@exit 1
endif
	npm uninstall $(PACKAGE_NAME) --force
	npm install $(PACKAGE_NAME)@latest --force

version:
ifndef PACKAGE_NAME
	@echo "Nie podano nazwy paczki. Wyświetlanie wersji dla wszystki paczek."
	npm list
else
	@echo "Wersja paczki $(PACKAGE_NAME)."
	npm list $(PACKAGE_NAME)
endif

outdated:
	@echo "Przedawnione wersje paczek npm."
	npm outdated

help:
ifndef INSTRUCTION
	@echo "Dostępne akcje: "
	@echo "make install - instaluje wszystkie paczki zawarte w pliku package.json"
	@echo "make update-package - instaluje podaną przez użytkownika paczkę."
	@echo "make version - pokazuje wersje dla podanej przez użytkownika paczki. Bez podania paczki pokazuje wersje wszystkich paczek."
	@echo "make outdated - pokazuje wszystkie paczki, których wersje są 'przedawnione'."
	@echo "By dowiedzieć się więcej na temat wybranej komendy należy podać jej wartość w ten sposób: make help INSTRUCTION_HELP=nazwa instrukcji."
else
ifeq ($(INSTRUCTION),install)
	@echo "make install - instaluje wszystkie paczki zawarte w pliku package.json"
else ifeq ($(INSTRUCTION),update-package)
	@echo "make update-package - instaluje w najnowszej wersji podaną przez użytkownika paczkę."
	@echo "Przykład użycia: make update-package PACKAGE_NAME=xxx, gdzie xxx to nazwa paczki npm."
else ifeq ($(INSTRUCTION),version)
	@echo "make version - pokazuje wersje dla podanej przez użytkownika paczki. Bez podania paczki pokazuje wersje wszystkich paczek."
	@echo "Przykład użycia: make version, lub make version PACKAGE_NAME=xxx, gdzie xxx to nazwa paczki npm."
else ifeq ($(INSTRUCTION),outdated)
	@echo "make outdated - pokazuje wszystkie paczki, których wersje są 'przedawnione'."
else
	@echo "Nie znaleziono instrukcji."
endif
endif
