# auth

## Projekt frontendowy do autoryzacji, wykorzystywany w różnych projektach.

### Polega on zewnętrznych paczkach npm, które należy ściągnąć by móc lokalnie działać. 
### Wywołanie komendy
```shell
make install
```
### Pobierze wszystkie paczki zawarte w pliku package.json.
### Więcej instrukcji jest dostępnych przy użyciu komendy:
```shell
make help
```


## Poszczególne foldery odpowiadają za inne rzeczy związane z serwisem frontendowym do logowania.

- API
  - Odpowiada za setup całego modułu autoryzacji.
- Components
  - Odpowiada na razie tylko za załączenie się procesu autowylogowania oraz jego egzekucji bez wysłania żądania o przedłużenie sesji.
- Models
  - Model ze zdarzeniami związanymi z autoryzacją.
- Services
  - Serwisy związane z autoryzacją.