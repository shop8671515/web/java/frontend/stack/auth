import { AccountsApi, MeApi, SecureAccountsApi, UsersApi } from "@ryszardszewczyk2204/user";
import { BaseAPI } from "@ryszardszewczyk2204/user/dist/base";

export declare class UserDefaultApi extends BaseAPI {
    usersApi: UsersApi
    meApi: MeApi
    accountsApi: AccountsApi
    secureAccountsApi: SecureAccountsApi
}