import { TokensApiFactory } from '@ryszardszewczyk2204/auth';
import {UserDefaultApi} from "./api";
import axios from 'axios'

interface AuthApi {
    auth: {
        default: ReturnType<typeof TokensApiFactory>,
        accountController: UserDefaultApi;
    };
    user: UserDefaultApi;
}

const authApiFactoryFallback: any = () => {
    throw new Error('Auth api has not been set');
};

const API: AuthApi = {
    auth: {
        default: authApiFactoryFallback as ReturnType<typeof TokensApiFactory>,
        accountController: authApiFactoryFallback as UserDefaultApi
    },
    user: authApiFactoryFallback as UserDefaultApi
};

export function setAuthApi(API_PORTAL_URL: string) {
    API.auth = {
        default: TokensApiFactory({ isJsonMime: () => true }, API_PORTAL_URL, axios),
        accountController: new UserDefaultApi({ isJsonMime: () => true }, API_PORTAL_URL, axios)
    };
    API.user = new UserDefaultApi({ isJsonMime: () => true }, API_PORTAL_URL, axios);
}

export default API;