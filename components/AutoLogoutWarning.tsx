import { useEffect, useState } from "react";
import { useTranslation } from 'react-i18next';
import { makeStyles } from "@mui/styles";
import AuthStorageService from "../services/AuthStorageService";
import AuthService from "../services/AuthService";

function AutoLogoutWarning({ onClose }: { onClose: () => void }) {
    const classes = useStyles();
    const [t] = useTranslation();
    const initialTimeInSeconds = 30;
    const [timeLeft, setTimeLeft] = useState(initialTimeInSeconds);
    const [timeoutId, setTimeoutId] = useState<NodeJS.Timeout>(null);
    const authTokePayload = AuthStorageService.getAuthTokenPayload();
    const { resetTimeout } = useAutoLogout();

    useEffect(() => {
        if (timeLeft > 1) {
            setTimeoutId(setTimeout(() => countDown(), 1000));
        } else {
            onClose();
        }
    }, [timeLeft]);

    useEffect(() => {
        window.removeEventListener('click', resetTimeout);
        window.removeEventListener('keydown', resetTimeout);
        return () => {
            onClose();
            clearTimeout(timeoutId);
        };
    }, []);

    const countDown = () => {
        setTimeLeft(timeLeft - 1);
    }

    const handleExtendSession = () => {
        window.addEventListener('click', resetTimeout);
        window.addEventListener('keydown', resetTimeout);
        AuthService.handleTokenRefresh(authTokePayload);
        onClose();
    };

    return (
        <div>
            {}
            {t('message.autoLogoutWarning.autoLogout', {
                count: timeLeft
            })}
            <span className={classes.action} onClick={handleExtendSession}>{t('action.extendSession')}</span>
        </div>
    );
}

const useStyles = makeStyles(() => ({
    action: {
        marginLeft: 4,
        textDecoration: "underline",
        cursor: "pointer"
    }
}));

export default AutoLogoutWarning;