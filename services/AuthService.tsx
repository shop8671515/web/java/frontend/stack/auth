import { NavigateFunction } from 'react-router-dom';
import { AccessTokenCreateRequest, AccessTokenDetails } from "@ryszardszewczyk2204/auth";
import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import moment from 'moment';

import AuthStorageService from "./AuthStorageService";
import EventEmitter = require("events");

import API from "../api/endpoints";

import { AuthEventsEnum } from "../models/auth-events.model";
import {QueryCache} from "react-query";
import {clearTimeout} from "timers";

type ToastrProps = (message: string, duration?: number) => void;

interface ErrorViolation {
    field: string;
    message: string;
    type: string;
}

interface CustomAxiosResponse<T = any> extends AxiosResponse<T> {
    config: CustomAxiosRequestConfig;
}

interface CustomAxiosRequestConfig extends AxiosRequestConfig {
    translationKey?: number;
}

class AuthService extends EventEmitter {
    private logoutWarningTimeoutID?: NodeJS.Timeout;
    public autoLogin?: boolean;
    public alreadyLogout?: boolean;
    private platform: string | null;
    private readonly _tokenExpirationDate: string;
    private readonly _authTokenPayload: AccessTokenDetails;
    private showErrorToastr?: ToastrProps;
    private showWarningToastr?: ToastrProps;
    private showInfoToastr?: ToastrProps;
    private navigate?: NavigateFunction;
    private broadcastLogout?: BroadcastChannel;

    constructor() {
        super();
        this.logoutWarningTimeoutID = undefined;
        this.autoLogin = true;
        this.alreadyLogout = false;
        this.platform = null;
        this._tokenExpirationDate = AuthStorageService.getTokenExpirationDate();
        this._authTokenPayload = AuthStorageService.getAuthTokenPayload();
        this.showErrorToastr = null;
        this.showWarningToastr = null;
        this.showInfoToastr = null;
        this.navigate = null;
    }

    init(
        platform: string,
        autoLogin?: boolean,
        showErrorToastr?: ToastrProps,
        showWarningToastr?: ToastrProps,
        showInfoToastr?: ToastrProps,
        navigate?: NavigateFunction
    ) {
        this.platform = platform;
        this.showErrorToastr = showErrorToastr;
        this.showWarningToastr = showWarningToastr;
        this.showInfoToastr = showInfoToastr;
        this.navigate = navigate;
        this.broadcastLogout = new BroadcastChannel('logout');
    }

    setLogoutBroadcast = (onmessage: (e: MessageEvent<any>) => void) => {
        if (!this.broadcastLogout) {
            this.broadcastLogout = new BroadcastChannel('logout');
        }
        this.broadcastLogout.addEventListener('message', onmessage);
    }

    sendLogoutBroadcast = () => {
        this.broadcastLogout?.postMessage(sessionStorage.getItem('sessionId'));
    };

    get tokenExpirationDate(): string {
        return this._tokenExpirationDate;
    }

    get authTokenPayload(): AccessTokenDetails {
        return this._authTokenPayload;
    }

    handleAuthentication = (autoLogin: boolean) => {
        if (autoLogin === false) {
            this.emit(AuthEventsEnum.LOGIN);
        } else {
            const authTokenPayload = AuthStorageService.getAuthTokenPayload();

            if (!authTokenPayload.accessToken) {
                this.emit(AuthEventsEnum.NO_ACCESS_TOKEN);

                return;
            }

            if (this.isAuthTokenValid()) {
                this.setSession(authTokenPayload);
                this.emit(AuthEventsEnum.AUTO_LOGIN, true);
            }
        }
    };

    login = (credentials: AccessTokenCreateRequest) => {
        return new Promise((resolve, reject) => {
            API.auth.default
                .create(credentials)
                .then(response => {
                    this.setSession(response.data, true);
                    this.alreadyLogout = false;
                    resolve(response);
                    this.emit(AuthEventsEnum.LOGIN);
                }).catch(error => {
                    console.error('Login error: ', error);
                    reject(new Error('Failed to login with token.'));
            });
        });
    };

    logout = (queryCache: QueryCache, redirectOptions = null) => {
        const accessToken = AuthStorageService.getAuthTokenPayload().accessToken ?? '';

        this.setSession(null);
        if (accessToken) {
            this.navigate('/login', { state: redirectOptions });

            return new Promise((resolve, reject) => {
                API.auth.default
                    .revoke(accessToken)
                    .then(response => {
                        this.emit(AuthEventsEnum.LOGOUT);
                        clearTimeout(this.logoutWarningTimeoutID);
                        queryCache.clear();
                        resolve(response);
                    })
                    .catch(() => {
                        reject(new Error('Failed to delete token.'));
                    });
            });
        }
        return Promise.resolve();
    }

    setSession = (authTokenPayload: AccessTokenDetails | null, saveToLocalStorage?: boolean) => {
        if (authTokenPayload && authTokenPayload.accessToken) {
            AuthStorageService.setAuthTokenPayload(authTokenPayload);

            if (saveToLocalStorage) {
                AuthStorageService.setAuthTokenPayload(authTokenPayload);
            }

            axios.defaults.headers.common['Authorization'] = `Bearer ${authTokenPayload.accessToken}`;
            axios.defaults.headers.common['platform'] = this.platform;
        }
    };

    isAuthTokenValid = (): boolean => {
        if (this.tokenExpirationDate) {
            return moment().isBefore(moment(this.tokenExpirationDate));
        }
        return false;
    };
}

const instance = new AuthService();

export default instance;