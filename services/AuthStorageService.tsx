import { AccessTokenDetails } from "@ryszardszewczyk2204/auth";
import moment from "moment";

export enum AuthPayloadType {
    ACCESS_TOKEN = 'access_token',
    REFRESH_TOKEN = 'refresh_token',
    EXPIRES_IN_SECONDS = 'expires_in_seconds',
    TOKEN_EXPIRATION_DATE = 'token_expiration_date'
}

class AuthStorageService {
    private readonly _localStorage: Storage;

    constructor() {
        this._localStorage = localStorage;
    }

    get localStorage(): Storage {
        return this._localStorage;
    }

    private getItem(key: string) {
        return this.localStorage.getItem(key);
    }

    private setItem(key: string, value: string | number) {
        this.localStorage.setItem(key, typeof value === 'number' ? value.toString(): value);
    }

    private removeItem(key: string) {
        this.localStorage.removeItem(key);
    }

    public getAuthTokenPayload(): AccessTokenDetails {
        return {
            accessToken: this.getItem(AuthPayloadType.ACCESS_TOKEN),
            refreshToken: this.getItem(AuthPayloadType.REFRESH_TOKEN),
            expiresInSeconds: Number(this.getItem(AuthPayloadType.EXPIRES_IN_SECONDS))
        };
    }

    public getTokenExpirationDate(): string {
        return this.getItem(AuthPayloadType.TOKEN_EXPIRATION_DATE);
    }

    public setAuthTokenPayload(authTokenPayload: AccessTokenDetails) {
        this.setItem(AuthPayloadType.ACCESS_TOKEN, authTokenPayload.accessToken);
        this.setItem(AuthPayloadType.REFRESH_TOKEN, authTokenPayload.refreshToken);
        this.setItem(AuthPayloadType.EXPIRES_IN_SECONDS, authTokenPayload.expiresInSeconds);
        this.setItem(
            AuthPayloadType.TOKEN_EXPIRATION_DATE,
            moment().add(authTokenPayload.expiresInSeconds, 'seconds').format()
        );
    }

    public removeAuthTokenPayload() {
        this.removeItem(AuthPayloadType.ACCESS_TOKEN);
        this.removeItem(AuthPayloadType.REFRESH_TOKEN);
        this.removeItem(AuthPayloadType.EXPIRES_IN_SECONDS);
        this.removeItem(AuthPayloadType.TOKEN_EXPIRATION_DATE);
    }
}

const instance = new AuthStorageService();

export default instance;