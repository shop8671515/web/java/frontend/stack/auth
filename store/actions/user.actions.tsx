import { QueryCache } from "react-query";
import { AxiosPromise } from "axios";
import {AccountState, AccountStateLite, PersonalDataSnapshot} from "@ryszardszewczyk2204/user"

import AuthService from "../../services/AuthService";

import API from "../../api/endpoints";

export const SET_USER_DATA = "[USER] SET DATA";
export const USER_LOGGED_OUT = "[USER] LOGGED OUT";

interface ExtendedPersonalData extends PersonalDataSnapshot {
    displayName?: string;
    email?: string
}

export enum GetUserStateTypeEnum {
    DEFAULT,
    LITE
}

const getStateQuery = (stateType: GetUserStateTypeEnum) => {
    switch (stateType) {
        case GetUserStateTypeEnum.LITE:
            return API.user.meApi.getLiteState()
        default:
            return API.user.meApi.getState()
    }
};

export function getUserData(params?: object | string[], userSchema?: object, getUserStateType?: GetUserStateTypeEnum): (dispatch: any) => Promise<void> {
    type AxiosRequests = [AxiosPromise<AccountState | AccountStateLite>]
    const requests = [getStateQuery(getUserStateType)]
    return dispatch =>
        Promise.all(requests as AxiosRequests).then(([accountStateResponse]) => {
            const { data } = accountStateResponse;
            const accountData = 'account' in data ? data.account : accountStateResponse.data || {};
            const personalData: ExtendedPersonalData = {
                ...('personalData' in data ? data.personalData : data)
            };
            personalData.displayName = `${personalData?.name ?? ''} ${personalData?.lastname ?? ''}`;

            const user = userSchema || {
                personalData,
                accountData,
                role: 'admin'
            };

            dispatch({
                type: SET_USER_DATA,
                payload: user
            });
        });
}

export function updateUserShortcuts(shortcuts: object) {
    return (dispatch, getState) => {
        const { user } = getState().auth as { user: { data: object } };
        const newUser = {
            ...user,
            data: {
                ...user.data,
                shortcuts
            }
        };

        updateUserData(newUser);

        return dispatch(getUserData(newUser));
    }
}

export function sendLogoutMsgToChannel() {
    AuthService.sendLogoutBroadcast();
}

export function logoutUser(queryCache: QueryCache, redirectOptions = null) {
    return (dispatch: any) => {
        AuthService.logout(queryCache, redirectOptions);
        dispatch({ type: USER_LOGGED_OUT });

        return Promise.resolve();
    }
}

function updateUserData(user: any) {
    if (!user.role || user.role.length === 0) {

    }
}